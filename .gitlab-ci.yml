---
stages:
  - build
  - image
  - test
  - release

default:
  interruptible: false
  image: continuumio/miniconda3:4.9.2

variables:
  CPP_TANGO_VERSION: "9.3.4"
  CACHE_FALLBACK_KEY: "$CI_DEFAULT_BRANCH"
  TWINE_USERNAME: __token__
  TWINE_PASSWORD: secret


cache:
  key: "$CI_COMMIT_REF_SLUG"
  untracked: false
  paths:
    - ./.eggs      # pytest eggs
    - ./build      # Build environment
    - ./envs
    - ./cached_ext
  policy: pull-push

.base_build:
  stage: test
  tags:
    - gitlab-org
  variables:
    TANGO_DEPENDENCIES: 'cpptango==${CPP_TANGO_VERSION} tango-test==3.2'
    DEBIAN_FRONTEND: "noninteractive"
  before_script:
    - apt-get update -y --allow-releaseinfo-change-suite
    - apt-get -y install make pkg-config rsync
    - conda init bash
    - source ~/.bashrc
    - conda activate ./envs/$PYTHON_VERSION || conda create --prefix ./envs/$PYTHON_VERSION --yes python=$PYTHON_VERSION
    - conda activate ./envs/$PYTHON_VERSION

    # Install build dependencies
    - conda install --yes -c main -c conda-forge boost gxx_linux-64 cppzmq numpy
    - conda install --yes -c main -c conda-forge -c tango-controls $TANGO_DEPENDENCIES
    # Use conda prefix as root for the dependencies
    - export BOOST_ROOT=$CONDA_PREFIX TANGO_ROOT=$CONDA_PREFIX ZMQ_ROOT=$CONDA_PREFIX OMNI_ROOT=$CONDA_PREFIX
    # Use custom boost python library name to work with newer naming scheme
    - export BOOST_PYTHON_LIB=boost_python$BOOST_PYTHON

    # make sure old_ext exists
    - mkdir -p cached_ext
    # Touch the .so files if the extension hasn't changed
    - diff -r cached_ext ext && find build -name _tango*.so -printf "touching %p\n" -exec touch {} + || true

    - python setup.py build

    # The build directory has been updated, cached_ext needs to be synchronized too
    - rsync -a --delete ext/ cached_ext/

build-pypi-sdist-package:
  stage: build
  image: python:3.8
  script:
    - python setup.py check sdist
  artifacts:
    expire_in: 1 day
    paths:
      - dist/

.base_build_docker_image:
  stage: image
  image: docker:latest
  services:
    - docker:dind
  only:
    refs:
      - tags
  variables:
    DOCKER_TLS_CERTDIR: "/certs"
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - export COMMIT_TAG=$(echo $CI_COMMIT_TAG | sed --expression="s/v//g")
    - export LOWER_PROJECT_NAMESPACE=$(echo ${CI_PROJECT_NAMESPACE} | tr '[:upper:]' '[:lower:]')
    - cd .devcontainer # Minimize build context migrated to docker container
    - docker build -t $CI_REGISTRY/${LOWER_PROJECT_NAMESPACE}/$CI_PROJECT_NAME/pytango-dev:py${PYTHON_VERSION}-tango${CPP_TANGO_VERSION}-pytango${COMMIT_TAG} --build-arg PYTHON_VERSION --build-arg CPP_TANGO_VERSION -f Dockerfile .
    - docker push $CI_REGISTRY/${LOWER_PROJECT_NAMESPACE}/$CI_PROJECT_NAME/pytango-dev:py${PYTHON_VERSION}-tango${CPP_TANGO_VERSION}-pytango${COMMIT_TAG}

build-docker-image-py27:
  extends: .base_build_docker_image
  variables:
    PYTHON_VERSION: "2.7"

build-docker-image-py37:
  extends: .base_build_docker_image
  variables:
    PYTHON_VERSION: "3.7"

build-docker-image-py38:
  extends: .base_build_docker_image
  variables:
    PYTHON_VERSION: "3.8"

test_py3.8:
  extends: .base_build
  variables:
    PYTHON_VERSION: '3.8'
    BOOST_PYTHON: '38'
  script:
    - python setup.py test

test_py3.7:
  extends: .base_build
  variables:
    PYTHON_VERSION: '3.7'
    BOOST_PYTHON: '37'
  script:
    - python setup.py test

test_py3.6:
  extends: .base_build
  variables:
    PYTHON_VERSION: '3.6'
    BOOST_PYTHON: '36'
  script:
    - python setup.py test

test_py3.5:
  extends: .base_build
  variables:
    PYTHON_VERSION: '3.5'
    BOOST_PYTHON: '35'
  script:
    - python setup.py test

test_py2.7:
  extends: .base_build
  variables:
    PYTHON_VERSION: '2.7'
    BOOST_PYTHON: '27'
  script:
    - python setup.py test

release-pypi-package:
  stage: release
  image: python:3.8
  before_script:
    - pip install twine
  script:
    - twine upload dist/*
  only:
    - tags
  when: manual
